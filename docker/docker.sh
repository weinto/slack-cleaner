#! /bin/bash

if ! command -v docker-compose &> /dev/null
then
    echo "Command 'docker-compose' could not be found. Did you install Docker?"
    exit 1
fi

export DOCKER_USER="$USER"
export DOCKER_UID="$(id -u $DOCKER_USER)"
export DOCKER_GID="$(id -g $DOCKER_USER)"

echo "Running docker-compose with user $DOCKER_USER, UID=$DOCKER_UID, GUID=$DOCKER_GID"

source .env
