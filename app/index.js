const { WebClient } = require('@slack/web-api');
const throttledQueue = require('throttled-queue');

const token = process.env.SLACK_TOKEN
const me = process.env.SLACK_USER_ID
const throttleDelay = 1200
const throttle = throttledQueue( 1, throttleDelay )
const slackWebClient = new WebClient(token)

function log_error(error) {
  console.log('[ERROR]', error)
}

async function conversationHistory(client, channelId, nextCursor) {
  try{
    const history = await client.conversations.history({
      channel: channelId,
      cursor: nextCursor,
      limit: 200
    })
    return history
  } catch (error) {
    log_error(error)
  }
}

async function collectConversationHistory(client, channelId, nextCursor = '') {

  let messages = []
  const history = await conversationHistory(client, channelId, nextCursor)

  messages = messages.concat(history.messages)

  if(history.has_more) {

    let nextPageMessages = await collectConversationHistory(
      client,
      channelId,
      history.response_metadata.next_cursor
    )

    messages = messages.concat(nextPageMessages)
  }

  return messages
}

async function conversationReplies(client, channelId, timestamp, nextCursor) {
  try{
    const history = await client.conversations.replies({
      channel: channelId,
      ts: timestamp,
      cursor: nextCursor,
      limit: 200
    })
    return history
  } catch (error) {
    log_error(error)
  }
}

async function collectConversationReplies(client, channelId, timestamp, nextCursor = '') {

  let messages = []
  const replies = await conversationReplies(client, channelId, timestamp, nextCursor)

  messages = messages.concat(replies.messages)

  if(replies.has_more) {

    let nextPageMessages = await collectConversationReplies(
      client,
      channelId,
      timestamp,
      replies.response_metadata.next_cursor
    )

    messages = messages.concat(nextPageMessages)
  }

  return messages
}

async function deleteFileComments(client, fileId, comments) {

  for(let c = 0; c<comments.length; c++) {
    let comment = comments[c]
    throttle(async () => {
      try {
        me === comment.user && await client.files.comments.delete({
          file: fileId,
          id: comment.id
        })
      } catch (error) {
        log_error(error)
      }
    })
  }
}

async function deleteFiles(client, files) {

  for(let f = 0; f<files.length; f++) {
    let file = files[f]

    if(file.mode !== 'tombstone') {
        let fileInfo = await client.files.info({
          file: file.id,
          count: 200
        })

        if(fileInfo.hasOwnProperty('comments')) {
          await deleteFileComments(client, file.id, fileInfo.comments)
        }

        throttle(async () => {
          if(me === file.user) {
            try {
              await client.files.delete({
                file: file.id
              })
            } catch (error) {
              console.log(file)
              log_error(error)
            }
          }
        })
    }

  }
}

async function deleteMessage(client, conversationId, timestamp) {
  throttle(async () => {
    try {
      await client.chat.delete({
        channel: conversationId,
        ts: timestamp
      })
    } catch (error) {
      log_error(error)
    }
  })
}

async function processConversationReplies(client, conversationId, replies) {

  for(let m = 0; m<replies.length; m++) {
    let reply = replies[m]

    if(reply.hasOwnProperty('files')) {
      await deleteFiles(client, reply.files)
    }

    if(me === reply.user) {
        await deleteMessage(client, conversationId, reply.ts)
    }
  }
}

async function processConversationHistory(client, conversationId, messages) {

  for(let m = 0; m<messages.length; m++) {
    let message = messages[m]

    if(message.hasOwnProperty('files')) {
      await deleteFiles(client, message.files)
    }

    if(message.reply_count > 0) {
      let replies = await collectConversationReplies(client, conversationId, message.ts)
      await processConversationReplies(slackWebClient, conversationId, replies)
    }

    if(me === message.user) {
      await deleteMessage(client, conversationId, message.ts)
    }
  }
}

(async () => {

  const conversations = await slackWebClient.users.conversations({
    types: 'im,mpim'
  })

  for(let c = 0; c<conversations.channels.length; c++) {
    let conversation = conversations.channels[c]

    let name = ''
    let id = conversation.id

    if(conversation.is_mpim) {
      console.log('mpim', conversation.name)
      name = conversation.name
    }
    if(conversation.is_im) {
      let user = await slackWebClient.users.info({
        user: conversation.user
      });

      console.log('im', user.user.name)
      name = user.user.name
    }

    console.log(id)
    let messages = await collectConversationHistory(slackWebClient, id)
    console.log(messages.length)
    await processConversationHistory(slackWebClient, id, messages)
  }
})();
