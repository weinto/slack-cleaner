# Slack cleaner

This project aims to help people to delete all private messages in Slack
workspaces because Slack does not delete a user account. They only "disable" it,
which means private conversations remain.

Public messages from Channels are note deleted but [it can be easily
added](https://api.slack.com/methods/users.conversations) at `index.js:192`.

```diff
- types: 'im,mpim'
+ types: 'im,mpim,channels,groups'
```

## Disclamer

The code isn't made to be beautiful or bug free. This app has been created in a
couple of hours (including learning the Slack API). It works. Period.

## 1. Create a Slack app

1. Go to <https://api.slack.com/apps/>
2. Click on "Create New App"
3. Allow read and write permission to the user's scope.

## 2. Lauch

Set environment variables in `docker/.env` from the sample file
`docker/sample.env`.

Then run the program from your terminal:

```bash
cd docker
bash run.docker.sh

docker exec -it weinto-slackcleaner_app_1 bash

node index.js
```
